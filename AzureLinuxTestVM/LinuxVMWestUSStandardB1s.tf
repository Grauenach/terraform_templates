#https://github.com/terraform-providers/terraform-provider-azurerm

provider "azurerm" {
    subscription_id = "*"
    client_id       = "*"
    client_secret   = "*"
    tenant_id       = "*"
}

resource "azurerm_public_ip" "myterraformpublicip" {
    name                         = "TerraformTestClientIP"
    location                     = "westus"
    resource_group_name          = "RealTestRG"
    public_ip_address_allocation = "dynamic"
	
}


resource "azurerm_network_security_group" "myterraformnsg" {
    name                = "TerraformTestClientNSG"
    location            = "westus"
    resource_group_name = "RealTestRG"

    security_rule {
        name                       = "SSH"
        priority                   = 1001
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "22"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }
}

resource "azurerm_network_interface" "myterraformnic" {
    name                = "TerraformTestClientNIC"
    location            = "westus"
    resource_group_name = "RealTestRG"
    network_security_group_id = "${azurerm_network_security_group.myterraformnsg.id}"

    ip_configuration {
        name                          = "myNicConfiguration"
        subnet_id                     = "default"
        private_ip_address_allocation = "dynamic"
        public_ip_address_id          = "${azurerm_public_ip.myterraformpublicip.id}"
    }
}


# Create virtual machine
resource "azurerm_virtual_machine" "myterraformvm" {
    name                  = "TerraformTestClientVM"
    location              = "westus"
    resource_group_name   = "RealTestRG"
    network_interface_ids = ["${azurerm_network_interface.myterraformnic.id}"]
    vm_size               = "Standard_B1s"

    storage_os_disk {
        name              = "TerraformTestClientVMOSDisk"
		vhd_uri			  = "https://realtestrgsa.blob.core.windows.net/vhds/TerraformTestClientVMOSDisk.vhd"
        caching           = "ReadWrite"
        create_option     = "FromImage"
    }

    storage_image_reference {
        publisher = "Canonical"
        offer     = "UbuntuServer"
        sku       = "16.04.0-LTS"
        version   = "latest"
    }

    os_profile {
        computer_name  = "myvm"
        admin_username = "TerraformTestClientVMAdmin"
		admin_password = "unlikemeNeighbourUncle"
    }

    os_profile_linux_config {
        disable_password_authentication = false
    }
}
