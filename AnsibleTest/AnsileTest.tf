#https://docs.microsoft.com/en-us/azure/virtual-machines/linux/ansible-install-configure
provider "azurerm" {
    subscription_id = ""
    client_id       = ""
    client_secret   = ""
    tenant_id       = ""
}


resource "azurerm_resource_group" "myterraformgroup" {
    name     = "AnsibleTestRG"
    location = "westus"
}



resource "azurerm_virtual_network" "myterraformnetwork" {
    name                = "AnsibleTestVnet"
    address_space       = ["10.0.0.0/16"]
    location            = "westus"
    resource_group_name = "${azurerm_resource_group.myterraformgroup.name}"
}



resource "azurerm_subnet" "myterraformsubnet" {
    name                 = "AnsibleTestSubnet"
    resource_group_name  = "${azurerm_resource_group.myterraformgroup.name}"
    virtual_network_name = "${azurerm_virtual_network.myterraformnetwork.name}"
    address_prefix       = "10.0.2.0/24"
}


resource "azurerm_public_ip" "myterraformpublicip1" {
    name                         = "AnsibleTestIP1"
    location                     = "westus"
    resource_group_name          = "${azurerm_resource_group.myterraformgroup.name}"
    public_ip_address_allocation = "dynamic"
}

resource "azurerm_public_ip" "myterraformpublicip2" {
    name                         = "AnsibleTestIP2"
    location                     = "westus"
    resource_group_name          = "${azurerm_resource_group.myterraformgroup.name}"
    public_ip_address_allocation = "dynamic"
}

resource "azurerm_network_security_group" "myterraformnsg" {
    name                = "AnsibleTestNSG"
    location            = "westus"
    resource_group_name = "${azurerm_resource_group.myterraformgroup.name}"

    security_rule {
        name                       = "SSH"
        priority                   = 1001
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "22"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }
}


resource "azurerm_network_interface" "myterraformnic1" {
    name                = "AnsibleTestNIC1"
    location            = "westus"
    resource_group_name = "${azurerm_resource_group.myterraformgroup.name}"
    network_security_group_id = "${azurerm_network_security_group.myterraformnsg.id}"

    ip_configuration {
        name                          = "AnsibleTestNicConfiguration1"
        subnet_id                     = "${azurerm_subnet.myterraformsubnet.id}"
        private_ip_address_allocation = "dynamic"
        public_ip_address_id          = "${azurerm_public_ip.myterraformpublicip1.id}"
    }
}


resource "azurerm_network_interface" "myterraformnic2" {
    name                = "AnsibleTestNIC2"
    location            = "westus"
    resource_group_name = "${azurerm_resource_group.myterraformgroup.name}"
    network_security_group_id = "${azurerm_network_security_group.myterraformnsg.id}"

    ip_configuration {
        name                          = "AnsibleTestNicConfiguration2"
        subnet_id                     = "${azurerm_subnet.myterraformsubnet.id}"
        private_ip_address_allocation = "dynamic"
        public_ip_address_id          = "${azurerm_public_ip.myterraformpublicip2.id}"
    }
}




resource "azurerm_storage_account" "mystorageaccount" {
    name                = "ansible1test42412x"
    resource_group_name = "${azurerm_resource_group.myterraformgroup.name}"
    location            = "westus"
    account_replication_type = "LRS"
    account_tier = "Standard"
}



# Create virtual machine
resource "azurerm_virtual_machine" "myterraformvm1" {
    name                  = "AnsibleTestHostVM"
    location              = "westus"
    resource_group_name   = "${azurerm_resource_group.myterraformgroup.name}"
    network_interface_ids = ["${azurerm_network_interface.myterraformnic1.id}"]
    vm_size               = "Standard_B1s"
	delete_data_disks_on_termination = true

    storage_os_disk {
        name              = "AnsibleTestHostVMOSDisk"
        caching           = "ReadWrite"
        create_option     = "FromImage"
		vhd_uri			  = "https://ansible1test42412x.blob.core.windows.net/vhds/AnsibleTestHostVMOSDisk.vhd"
    }

    storage_image_reference {
        publisher = "Canonical"
        offer     = "UbuntuServer"
        sku       = "16.04.0-LTS"
        version   = "latest"
    }

    os_profile {
        computer_name  = "AnsibleTestHostVM"
        admin_username = "AnsibleTestHostVMAdmin"
		admin_password = "unlikemeNeighbourUncle@12"
    }

    os_profile_linux_config {
        disable_password_authentication = false
    }
}


resource "azurerm_virtual_machine" "myterraformvm2" {
    name                  = "AnsibleTestNodeVM"
    location              = "westus"
    resource_group_name   = "${azurerm_resource_group.myterraformgroup.name}"
    network_interface_ids = ["${azurerm_network_interface.myterraformnic2.id}"]
    vm_size               = "Standard_B1s"
	delete_data_disks_on_termination = true

    storage_os_disk {
        name              = "AnsibleTestNodeVMOSDisk"
        caching           = "ReadWrite"
        create_option     = "FromImage"
		vhd_uri			  = "https://ansible1test42412x.blob.core.windows.net/vhds/AnsibleTestNodeVMOSDisk.vhd"
    }

    storage_image_reference {
        publisher = "Canonical"
        offer     = "UbuntuServer"
        sku       = "16.04.0-LTS"
        version   = "latest"
    }

    os_profile {
        computer_name  = "AnsibleTestNodeVM"
        admin_username = "AnsibleTestNodeVMAdmin"
		admin_password = "unlikemeNeighbourUncle@12"
    }

    os_profile_linux_config {
        disable_password_authentication = false
    }
}
